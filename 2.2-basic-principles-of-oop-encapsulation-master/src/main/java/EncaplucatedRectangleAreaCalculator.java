public class EncaplucatedRectangleAreaCalculator {

    public static void main(String[] args) {
        EncapsulatedRectangle rectangle = new EncapsulatedRectangle();
        rectangle.setSides(4, 5);
        int area = rectangle.getArea();
        System.out.println("Площа прямокутника зі сторонами " + rectangle.getSideA() + " та " + rectangle.getSideB() +": " + area);
    }
}

class EncapsulatedRectangle {

    private int sideA;
    private int sideB;

    public void setSides(int sideA,int sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public int getSideA() {
        return sideA;
    }

    public int getSideB() {
        return sideB;
    }

    public int getArea() {
        return sideA * sideB;
    }
}
public class Human {

    public int age;
    public String gender;

    public void sleep() {
        System.out.println("I'm sleeping! Z-z-z...");
    }

    public void drink() {
        System.out.println("Getting some water");
    }

    public void think() {
        System.out.println("Ok, let me think");
    }
}

public class GeographyTeacher extends Teacher {

    public Object globe;

    @Override
    public void teach() {
        System.out.println("Teaching some knowledge about the Earth");
    }
}

import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Stream;

public class DataTypes<numbers, n> {
    public static void main(String[] args) {

       // мінімальне та максимальне значення масиву, кількість елементів в масиві, друк у зворотному порядку
        int[] tab = {34, 57, 7, 32, 98, 99, 12, 3};

        int min = Integer.MAX_VALUE;
        int max = 0;
        int k = tab.length;
        int numOfEl = 0;

        System.out.println("Inverse array: ");
        for (int i = tab.length - 1; i >= 0; i--) {
            System.out.print(tab[i] + " ");
        }

        for (int i = 0; i < tab.length; i++) {

            if (tab[i] > max) {
                max = tab[i];
            }

            if (tab[i] < min) {
                min = tab[i];
            }
        }

        for (int i = 0; i < tab.length; i++) {
            numOfEl = numOfEl + 1;
        }

        System.out.println();
        System.out.println("Num of elements: " + numOfEl);
        System.out.println("Min = " + min);
        System.out.println("Max = " + max);
        System.out.println("\n");

        // периметр п'ятикутника
        int[] data = new int[5];
        System.out.println("Please enter sides length of the pentagon:");
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 5; i++) {
            int num = scanner.nextInt();
            data[i] = num;
        }

        int perym = 0;
        for (int i = 0; i < 5; i++) {
            perym = perym + data[i];
        }
        System.out.println("The perimeter of the pentagon: " + perym);
        System.out.println("\n");


        // прибуток компанії
        int[] profit;
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter numbers of months: ");
        n = in.nextInt();
        profit = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Enter profit[" + i + "] = ");
            profit[i] = in.nextInt();
        }

        int min1 = Integer.MAX_VALUE;
        int max1 = 0;
        int x = profit.length;
        int indexOfMax = 0;
        int indexOfMin = 0;

        for (int i = 0; i < profit.length; i++) {

            if (profit[i] > max1) {
                max1 = profit[i];
                indexOfMax = i;
            }

            if (profit[i] < min1) {
                min1 = profit[i];
                indexOfMin = i;
            }
        }

        System.out.println();
        System.out.print("Min profit = " + min1 + ", ");
        System.out.println("The month index (min) = " + indexOfMin);
        System.out.print("Max profit = " + max1 + ", ");
        System.out.println("The month index (max) = " + indexOfMax);
        System.out.println("\n");

    }
    }












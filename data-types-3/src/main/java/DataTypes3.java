import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class DataTypes3 {
    public static void main(String[] args) {

        // метод бульбашок
        int[] mas = {15, 5, 11, 13, 8, 7, 3};

        boolean isSorted = false;
        int temp;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < mas.length - 1; i++) {
                if (mas[i] > mas[i + 1]) {
                    isSorted = false;

                    temp = mas[i];
                    mas[i] = mas[i + 1];
                    mas[i + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(mas));

        // метод бульбашок 2
        int[] array = {10, 3, 53, 2, 5, 7, 4, 6, 12, 33, 32, 65};
        int temp1;
        for (int i = array.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    temp1 = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp1;
                }
            }
        }
        System.out.println(Arrays.toString(array));


        // перемішування елементів масиву

        int[] arr = {10, 20, 30, 40, 50, 60, 70, 80, 90};
        int[] newArray = new int[arr.length];

        Random random = new Random();

        List<Integer> indexes = new ArrayList<>(arr.length);
        int count = 0;
        while (true) {
            int var = random.nextInt(arr.length);
            if (!indexes.contains(var)) {
                indexes.add(var);
                newArray[var] = arr[count++];
            }
            if (count == arr.length) {
                break;
            }
        }
        System.out.println(Arrays.toString(newArray));
    }
}
